from textwrap import indent
from time import sleep
import requests as r
import sqlalchemy as s
import json
import string
from sqlalchemy import Column, Integer, create_engine,Column,Integer,String,create_engine,Float
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


    
engine = create_engine("sqlite+pysqlite:///main.db",echo=False)

Session = sessionmaker(bind=engine)
session = Session()

Base=declarative_base()

def districtrainfall():
    __tablename__='rainfall'
    cid=Column(Integer,Primary_Key=True, nullable=False)
    SUBDIVISION=Column(String(1024))
    YEAR=Column(Integer())
    DISTRICT=Column(String(50))
    JAN=Column(String(50))
    FEB=Column(String(50))
    MAR=Column(String(50))
    APR=Column(String(50))
    MAY=Column(String(50))
    JUN=Column(String(50))
    JUL=Column(String(50))
    AUG=Column(String(50))
    SEP=Column(String(50))
    OCT=Column(String(50))
    NOV=Column(String(50))
    DEC=Column(String(50))
    JAN_FEB=Column(String(50))
    MAR_MAY=Column(String(50))
    JUN_SEP=Column(String(50))
    OCT_DEC=Column(String(50))
